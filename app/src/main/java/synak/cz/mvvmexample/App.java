package synak.cz.mvvmexample;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import synak.cz.mvvmexample.core.component.AppComponent;
import synak.cz.mvvmexample.core.component.DaggerAppComponent;
import synak.cz.mvvmexample.core.module.AppModule;
import timber.log.Timber;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

public class App
		extends Application {

	/* Private Types ******************************************************************************/

	/**
	 * {@link Timber.Tree} class used for logging in debug mode.
	 */
	private static class DebugTree
			extends Timber.DebugTree {

		@Override
		protected boolean isLoggable(int priority) {
			return (priority >= Log.VERBOSE);
		}

		@Override
		protected String createStackElementTag(StackTraceElement element) {
			return super.createStackElementTag(element) + ":" + element.getLineNumber();
		}

		@Override
		protected void log(int priority, String tag, String message, Throwable t) {
			switch (priority) {
				case Log.VERBOSE:
					Log.v(tag, message, t);
					break;
				case Log.DEBUG:
					Log.d(tag, message, t);
					break;
				case Log.INFO:
					Log.i(tag, message, t);
					break;
				case Log.WARN:
					Log.w(tag, message, t);
					break;
				case Log.ERROR:
					Log.e(tag, message, t);
					break;
			}
		}
	}

	/* Private Attributes *************************************************************************/

	private static AppComponent sAppComponent;

	/* Public Methods *****************************************************************************/

	@Override
	public void attachBaseContext(Context base) {
		MultiDex.install(base);
		super.attachBaseContext(base);
	}

	/**
	 * @see Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();

		if (!BuildConfig.DEBUG) {
			Fabric.with(this, new Crashlytics());
		} else {
			Timber.plant(new DebugTree());
		}

		Timber.d("onCreate()");

		sAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();

	}

	public static AppComponent getAppComponent() {
		return sAppComponent;
	}
}

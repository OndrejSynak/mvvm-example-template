package synak.cz.mvvmexample.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Base class for all application activities.
 */
public abstract class BaseActivity
		extends ViewModelEmptyActivity {

	/* Private Attributes *************************************************************************/



	/* Protected Methods **************************************************************************/

	protected void onCreate(@Nullable final Bundle savedInstanceState, int layoutId) {
		super.onCreate(savedInstanceState, layoutId);

	}

}

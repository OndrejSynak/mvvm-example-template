package synak.cz.mvvmexample.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import synak.cz.mvvmexample.R;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Class for managing drawer functionality.
 */


public abstract class DrawerActivity
		extends ToolbarActivity {

	/* Ui Components ******************************************************************************/

	@Nullable
	//@BindView(R.id.drawer_layout)
	DrawerLayout mDrawerLayout;

	/* Private Attributes *************************************************************************/

	private ActionBarDrawerToggle mDrawerToogle;

	@Override
	protected void onCreate(@Nullable final Bundle savedInstanceState, final int layoutId) {
		super.onCreate(savedInstanceState, layoutId);

		if (mDrawerLayout != null) {
			mDrawerToogle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {
				@Override
				public void onDrawerOpened(View drawerView) {
					super.onDrawerOpened(drawerView);
				}

				@Override
				public void onDrawerClosed(View drawerView) {
					super.onDrawerClosed(drawerView);
				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToogle);

			setupDrawerItems();
		}

	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		if (mDrawerToogle != null && mDrawerToogle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPostCreate(@Nullable final Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mDrawerToogle != null) {
			mDrawerToogle.syncState();
		}
	}

	/* Private methods ****************************************************************************/

	private void setupDrawerItems() {

	}
}

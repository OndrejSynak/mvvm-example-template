package synak.cz.mvvmexample.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;

import synak.cz.mvvmexample.R;
import synak.cz.mvvmexample.pages.main_page.IMainView;
import synak.cz.mvvmexample.pages.main_page.MainViewModel;
import timber.log.Timber;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

public class MainActivity
		extends ViewModelActivity<IMainView, MainViewModel>
		implements IMainView {

	/* UI Components ******************************************************************************/

	/* Public Methods *****************************************************************************/

	@Override
	public Class<MainViewModel> getViewModelClass() {
		return MainViewModel.class;
	}

	/* Protected Methods **************************************************************************/


	@SuppressLint("MissingSuperCall")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Timber.d("onCreate()");

		super.onCreate(savedInstanceState, R.layout.activity_main);

		// Initialize views.
		setupViews();
	}


	/* Private Methods ****************************************************************************/

	/**
	 * Setup views to initial values and states.
	 */
	private void setupViews() {
		Timber.d("setupViews()");

		setTitle("MainActivity");
	}

}

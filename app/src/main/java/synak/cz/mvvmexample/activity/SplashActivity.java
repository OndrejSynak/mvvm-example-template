package synak.cz.mvvmexample.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;

import synak.cz.mvvmexample.App;
import synak.cz.mvvmexample.R;
import synak.cz.mvvmexample.core.SharedPreferencesHelper;
import timber.log.Timber;

/**
 * Created by Ondrej Synak on 7/15/17.
 */

public class SplashActivity
		extends ViewModelEmptyActivity {

	/* UI Components ******************************************************************************/

	/* Public Constants ***************************************************************************/

	/* Private Attributes *************************************************************************/

	/* Public Methods *****************************************************************************/

	/* Protected Methods **************************************************************************/


	@SuppressLint("MissingSuperCall")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Timber.d("onCreate()");

		super.onCreate(savedInstanceState, R.layout.activity_splash);

		App.getAppComponent().provideSharedPreferencesHelper().saveBoolean(SharedPreferencesHelper.APP_INITIALIZED, true);

		// Initialize views.
		setupViews();
	}

	/* Private Methods ****************************************************************************/

	/**
	 * Setup views to initial values and states.
	 */
	private void setupViews() {
		Timber.d("setupViews()");

	}

}

package synak.cz.mvvmexample.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import synak.cz.mvvmexample.R;

/**
 * Created by Ondrej Synak on 7/13/17.
 *
 * Class for managing toolbar functionality.
 */


public abstract class ToolbarActivity
		extends BaseActivity {

	/* UI Components ******************************************************************************/

	@Nullable
	@BindView(R.id.toolbar)
	Toolbar mToolbar;

	/* Public Methods *****************************************************************************/

	/**
	 * @see BaseActivity#onOptionsItemSelected(MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Sets title of toolbar.
	 *
	 * @param title
	 */
	public void setTitle(String title) {
		if (mToolbar != null) {
			mToolbar.setTitle(title);
		}
	}

	@Override
	protected void onPostCreate(@Nullable final Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		if (mToolbar == null) {
			return;
		}
		setSupportActionBar(mToolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}
}


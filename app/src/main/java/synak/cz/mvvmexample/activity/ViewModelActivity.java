package synak.cz.mvvmexample.activity;

/**
 * Created by Ondrej Synak on 7/13/17.
 */


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import eu.inloop.viewmodel.IView;
import eu.inloop.viewmodel.ViewModelHelper;
import eu.inloop.viewmodel.binding.ViewModelBindingConfig;
import synak.cz.mvvmexample.viewmodel.BaseActivityViewModel;
import synak.cz.mvvmexample.viewmodel.IBaseView;

/**
 * Base abstract class for activities using view-model.
 * Always extend this class if activity has viewmodel.
 *
 * @param <TView> View interface type.
 * @param <TViewModel> View model type.
 */

public abstract class ViewModelActivity<TView extends IView, TViewModel extends BaseActivityViewModel<TView>>
		extends DrawerActivity
		implements IBaseView {

	/* Private Attributes *************************************************************************/

	/**
	 * View-model helper.
	 */
	private ViewModelHelper<TView, TViewModel> mViewModelHelper = new ViewModelHelper<>();

	/**
	 * Whether the view-model should be bound to view automatically when activity layout is created.
	 */
	private boolean mAutoSetModelView = true;

  	/* Public Abstract Methods ********************************************************************/

	/**
	 * Returns class of view-model coupled with this activity.
	 */
	public abstract Class<TViewModel> getViewModelClass();

  	/* Public Methods *****************************************************************************/

	/**
	 * Returns the activity view model.
	 *
	 * @return Activity view model.
	 */
	public TViewModel getViewModel() {
		return mViewModelHelper.getViewModel();
	}

	/**
	 * Configures whether the view-model should be bound to view automatically.
	 */
	public void setAutoSetModelView(boolean autoSetModelView) {
		mAutoSetModelView = autoSetModelView;
	}

	/**
	 * Call this after your view is ready - usually on the end of
	 * {@link FragmentActivity#onCreate(Bundle)}.
	 *
	 * @param view The view-model view.
	 */
	public void setModelView(@NonNull TView view) {
		mViewModelHelper.setView(view);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Shows the keyboard.
	 */
	public void showKeyboard(View view) {
		InputMethodManager manager = (InputMethodManager)
				getSystemService(Context.INPUT_METHOD_SERVICE);
		manager.showSoftInput(view, InputMethodManager.SHOW_FORCED);
	}

	/**
	 * Hides the keyboard, if shown.
	 */
	public void hideKeyboard() {
		InputMethodManager manager = (InputMethodManager)
				getSystemService(Context.INPUT_METHOD_SERVICE);
		View currentFocus = getCurrentFocus();
		if (currentFocus != null) {
			manager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
		}
	}

	/**
	 * Clears the focus from the currently focused view.
	 */
	public void clearFocus() {
		View currentFocus = getCurrentFocus();
		if (currentFocus != null) {
			currentFocus.clearFocus();
		}
	}

	@Override
	public void removeViewModel() {
		mViewModelHelper.removeViewModel(this);
	}

	@Nullable
	@Override
	public ViewModelBindingConfig getViewModelBindingConfig() {
		return null;
	}

	/* Protected Methods **************************************************************************/

	/**
	 * @see FragmentActivity#onCreate(Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState, int layoutId) {
		super.onCreate(savedInstanceState, layoutId);

		mViewModelHelper.onCreate(this, savedInstanceState,
				getViewModelClass(), getIntent().getExtras());
	}

	/**
	 * @see FragmentActivity#onPostCreate(Bundle)
	 */
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		if (mAutoSetModelView) {
			setModelView((TView) this);
		}
	}

	/**
	 * @see FragmentActivity#onStart()
	 */
	@Override
	protected void onStart() {
		super.onStart();
		mViewModelHelper.onStart();
	}

	/**
	 * @see FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		findViewById(android.R.id.content)
				.post(this::onLayoutCreated);
	}

	/**
	 * Called when activity layout is inflated.
	 */
	protected void onLayoutCreated() {
		getViewModel().onViewCreated();
	}

	/**
	 * @see FragmentActivity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		hideKeyboard();
	}

	/**
	 * @see FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		mViewModelHelper.onStop();
	}

	/**
	 * @see FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		mViewModelHelper.onDestroy(this);
		super.onDestroy();
	}

	/**
	 * @see FragmentActivity#onSaveInstanceState(Bundle)
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mViewModelHelper.onSaveInstanceState(outState);
	}

}
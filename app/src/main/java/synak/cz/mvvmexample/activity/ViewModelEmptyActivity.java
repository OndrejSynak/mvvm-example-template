package synak.cz.mvvmexample.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import eu.inloop.viewmodel.IViewModelProvider;
import eu.inloop.viewmodel.ViewModelProvider;
import synak.cz.mvvmexample.App;
import synak.cz.mvvmexample.core.SharedPreferencesHelper;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * All activities must extent this class even if they have no view model. The fragment view models
 * are using the {@link IViewModelProvider} interface to get the {@link ViewModelProvider} from the
 * current activity.
 */

public abstract class ViewModelEmptyActivity
		extends AppCompatActivity
		implements IViewModelProvider {

	/* Private Attributes *************************************************************************/

	/**
	 * Butterknife unbinder instance.
	 */
	private Unbinder mUnbinder;

	/**
	 * View-model provider.
	 */
	private ViewModelProvider mViewModelProvider;

    /* Public Methods *****************************************************************************/

	/**
	 * @see FragmentActivity#onRetainCustomNonConfigurationInstance()
	 */
	@Override
	@Nullable
	public Object onRetainCustomNonConfigurationInstance() {
		return mViewModelProvider;
	}

	/**
	 * @see IViewModelProvider#getViewModelProvider()
	 */
	@Override
	public ViewModelProvider getViewModelProvider() {
		return mViewModelProvider;
	}

  	/* Protected Methods **************************************************************************/

	/**
	 * @see FragmentActivity#onCreate(Bundle)
	 */
	protected void onCreate(@Nullable Bundle savedInstanceState, int layoutId) {
		mViewModelProvider = ViewModelProvider.newInstance(this);
		super.onCreate(savedInstanceState);

		// If this is initial launcher activity, start intro activity.
		if (getClass().getName().equals(MainActivity.class.getName())) {
			if (!App.getAppComponent().provideSharedPreferencesHelper().loadBoolean(SharedPreferencesHelper.APP_INITIALIZED)
					&& !getClass().getName().equals(SplashActivity.class.getName())) {
				this.startActivity(new Intent(this, SplashActivity.class));
			}
		}

		setContentView(layoutId);
		mUnbinder = ButterKnife.bind(this);
	}

	/**
	 * @see FragmentActivity#onStop()
	 */
	@Override
	protected void onStop() {
		super.onStop();
		if (isFinishing()) {
			mViewModelProvider.removeAllViewModels();
		}
	}


	@Override
	protected void onDestroy() {

		if (mUnbinder != null) {
			mUnbinder.unbind();
		}

		super.onDestroy();
	}


}

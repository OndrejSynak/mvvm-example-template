package synak.cz.mvvmexample.core;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Singleton;


/**
 * Singleton class for handling shared preferences operations.
 */

@Singleton
public class SharedPreferencesHelper
		extends ContextWrapper {

	/* Private Constants **************************************************************************/


	public static final String PREFERENCES_NAME = "synak.com.mwwmexample";

	/**
	 * Shared pref. keys.
	 */
	public static final String APP_INITIALIZED = "app_initialized";

	/* Private Attributes *************************************************************************/

	private Context mContext;
	private Gson mGson;
	private SharedPreferences mSharedPreferences;

	/* Constructors *******************************************************************************/

	public SharedPreferencesHelper(final Context context, final Gson gson) {
		super(context.getApplicationContext());
		this.mContext = context;
		this.mGson = gson;
		this.mSharedPreferences = getSharedPreferences(PREFERENCES_NAME, MODE_PRIVATE);
	}


	/* Public Methods *****************************************************************************/

	/**
	 * General Save function to shared preferences.
	 *
	 * @param data Saved data.
	 * @param key Shared pref. key
	 * @param <T> Generic Serialization class.
	 */
	public <T> void save(T data, String key) {
		mSharedPreferences.edit().putString(key, mGson.toJson(data)).apply();
	}

	/**
	 * General Load function from shared preferences.
	 *
	 * @param key Shared pref. key
	 * @param clazz Class for deserialization.
	 * @param <T>
	 *
	 * @return Data.
	 */
	public <T> T load(String key, Class<T> clazz) {
		return mGson.fromJson(mSharedPreferences.getString(key, ""), clazz);
	}

	public void saveBoolean(String key, boolean state) {
		mSharedPreferences.edit().putBoolean(key, state).apply();
	}

	public boolean loadBoolean(String key) {
		return mSharedPreferences.getBoolean(key, false);
	}

	public void saveInt(String key, int state) {
		mSharedPreferences.edit().putInt(key, state).apply();
	}

	public int loadInt(String key) {
		return mSharedPreferences.getInt(key, -1);
	}

	public void saveLong(String key, long state) {
		mSharedPreferences.edit().putLong(key, state).apply();
	}

	public long loadLong(String key) {
		return mSharedPreferences.getLong(key, -1);
	}

}

package synak.cz.mvvmexample.core.component;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import synak.cz.mvvmexample.core.SharedPreferencesHelper;
import synak.cz.mvvmexample.core.module.AppModule;
import synak.cz.mvvmexample.core.module.NetModule;
import synak.cz.mvvmexample.network.NetworkHelper;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {


	Context provideContext();

	Application provideApplication();

	OkHttpClient provideOkHttpClient();

	Retrofit provideRetrofit();

	Gson provideGson();

	Picasso providePicasso();

	NetworkHelper provideNetworkHelper();

	SharedPreferencesHelper provideSharedPreferencesHelper();
}

package synak.cz.mvvmexample.core.module;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import synak.cz.mvvmexample.core.SharedPreferencesHelper;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

@Module
public class AppModule {
	/* Private Attributes *************************************************************************/

	/**
	 * Application instance.
	 */
	private Application mApplication;

  	/* Public Methods *****************************************************************************/

	/**
	 * Constructor.
	 *
	 * @param application Application instance.
	 */
	public AppModule(Application application) {
		mApplication = application;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////


	@Provides
	@Singleton
	public Context provideContext() {
		return mApplication.getApplicationContext();
	}

	@Provides
	@Singleton
	public Application provideApplication() {
		return mApplication;
	}

	@Provides
	@Singleton
	public SharedPreferencesHelper provideSharedPreferencesHelper(final Context context, final Gson gson) {
		return new SharedPreferencesHelper(context, gson);
	}
}

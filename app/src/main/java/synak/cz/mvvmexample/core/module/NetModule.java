package synak.cz.mvvmexample.core.module;

import android.content.Context;
import android.support.compat.BuildConfig;

import com.google.gson.Gson;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;
import synak.cz.mvvmexample.R;
import synak.cz.mvvmexample.network.NetworkHelper;

/**
 * Created by Ondrej Synak on 7/13/17.
 */


@Module
public class NetModule {

	private static final String URL = "url";
	private static final int READ_TIMEOUT = 60;
	private static final int CONNECT_TIMEOUT = 60;

	@Provides
	@Singleton
	@Named(URL)
	static String provideUrl(final Context context) {
		return BuildConfig.DEBUG ? context.getString(R.string.debug_server_url) : context.getString(R.string.release_server_url);
	}

	@Provides
	@Singleton
	public OkHttpClient provideOkHttpClient(Context context) {
		return new OkHttpClient.Builder()
				.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
				.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
				.addInterceptor(chain -> {
					final Request request = chain.request();
					final String auth = request.header("Authorization");
					if (auth == null || auth.isEmpty()) {
						return chain.proceed(request.newBuilder().removeHeader("Authorization").build());
					}
					return chain.proceed(request);
				})
				.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)) // Must be last!
				.build();
	}

	@Provides
	@Singleton
	public Retrofit provideRetrofit(final OkHttpClient client, final @Named(URL) String url) {
		return new Retrofit.Builder()
				.baseUrl(url)
				.client(client)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
				.build();
	}

	@Provides
	@Singleton
	public Gson provideGson() {
		return new Gson();
	}

	@Provides
	@Singleton
	public Picasso providePicasso(final OkHttpClient client, final Context context) {
		return new Picasso.Builder(context)
				.downloader(new OkHttp3Downloader(client.newBuilder().cache(OkHttp3Downloader.createDefaultCache(context)).build()))
				.indicatorsEnabled(BuildConfig.DEBUG)
				.loggingEnabled(BuildConfig.DEBUG)
				.build();
	}

	@Provides
	@Singleton
	public NetworkHelper provideNetworkHelper(final Context context, final Gson gson, final Retrofit retrofit) {
		return new NetworkHelper(context, retrofit, gson);
	}
}
package synak.cz.mvvmexample.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;

import java.io.IOException;

import javax.inject.Singleton;

import retrofit2.Response;
import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Singleton for managing api calls.
 */

@Singleton
public class NetworkHelper {

	/* Public Constants **************************************************************************/

	/**
	 * Maximum retries of one request.
	 */
	public static final int MAX_RETRIES = 0;


	/* Private Attributes *************************************************************************/

	private Retrofit mRetrofit;
	private Context mContext;
	private Gson mGson;

	/* Constructor ********************************************************************************/

	public NetworkHelper(Context context, Retrofit retrofit, Gson gson) {
		mContext = context;
		mRetrofit = retrofit;
		mGson = gson;

		init();
	}

	/* Public Methods *****************************************************************************/

	/**
	 * Checks for network.
	 *
	 * @return
	 */
	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager
				= (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}


	/* Private Methods ****************************************************************************/

	private void init() {
		// Initialize all network services.

	}

	/**
	 * Check if response from server is successful
	 *
	 * @param response Response from server.
	 *
	 * @return Observable.onNext(response body) or Error in Observable.onError
	 */
	private <T> Observable<T> checkIfResponseIsSuccessful(Response<T> response) {
		if (response.isSuccessful()) {
			return Observable.just(response.body());
		} else {
			//try {
			//	//CallResult callResult = mGson.fromJson(response.errorBody().string(), CallResult.class);
			//	//
			//	//return Observable.error(new Throwable(callResult.getMessage()));
			//	return Observable.error(new Throwable());
			//} catch (IOException e) {
			//	return Observable.error(e);
			//}
			return Observable.error(null);
		}
	}
}

package synak.cz.mvvmexample.pages.main_page;

import synak.cz.mvvmexample.viewmodel.IBaseView;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

public interface IMainView
		extends IBaseView {
}

package synak.cz.mvvmexample.pages.main_page;

import android.os.Bundle;
import android.support.annotation.NonNull;

import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import synak.cz.mvvmexample.viewmodel.BaseActivityViewModel;
import timber.log.Timber;

/**
 * Created by Ondrej Synak on 7/13/17.
 */

public class MainViewModel
		extends BaseActivityViewModel<IMainView> {

	/* Private Attributes *************************************************************************/


	/* Public Methods *****************************************************************************/

	/**
	 * @see AbstractViewModel#onCreate(Bundle, Bundle)
	 */
	@Override
	public void onCreate(Bundle arguments, Bundle savedInstanceState) {
		Timber.d("onCreate()");

		super.onCreate(arguments, savedInstanceState);

		// Decide which state bundle should be used when loading arguments.
		Bundle state = (savedInstanceState != null) ? savedInstanceState : arguments;

		// Load arguments.
		loadArguments(state);
	}

	/**
	 * @see AbstractViewModel#onSaveInstanceState(Bundle)
	 */
	@Override
	public void onSaveInstanceState(@NonNull Bundle bundle) {
		Timber.d("onSaveInstanceState()");

		super.onSaveInstanceState(bundle);

	}

	/**
	 * @see AbstractViewModel#onBindView(IView)
	 */
	@Override
	public void onBindView(@NonNull IMainView view) {
		Timber.d("onBindView()");

		super.onBindView(view);
	}

	@Override
	public void onViewCreated() {
		super.onViewCreated();

	}

	/**
	 * @see AbstractViewModel#onDestroy()
	 */
	@Override
	public void onDestroy() {
		Timber.d("onDestroy()");

		super.onDestroy();
	}


 	/* Private Methods ****************************************************************************/

	/**
	 * Loads arguments from bundle.
	 *
	 * @param state State bundle.
	 */
	private void loadArguments(Bundle state) {
		// Load arguments.
		if (state != null) {

		}
	}

}

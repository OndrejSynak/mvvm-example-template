package synak.cz.mvvmexample.viewmodel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import synak.cz.mvvmexample.activity.ViewModelActivity;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Base class for all viewmodels.
 */

public abstract class BaseActivityViewModel<TView extends IView>
		extends AbstractViewModel<TView> {

	/* Protected Attributes ***********************************************************************/

	/**
	 * Link to view (FragmentActivity).
	 */
	protected ViewModelActivity mActivity;

	/* Public Methods *****************************************************************************/

	/**
	 * @see AbstractViewModel#onCreate(Bundle, Bundle)
	 */
	@Override
	public void onCreate(@Nullable Bundle arguments, @Nullable Bundle savedInstanceState) {
		super.onCreate(arguments, savedInstanceState);

	}

	/**
	 * @see AbstractViewModel#onBindView(IView)
	 */
	@Override
	public void onBindView(@NonNull TView view) {
		super.onBindView(view);

		// Remember the link to activity.
		mActivity = (ViewModelActivity) view;

	}

	/**
	 * @see AbstractViewModel#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
	}

	/**
	 * @see AbstractViewModel#clearView()
	 */
	@Override
	public void clearView() {
		super.clearView();
	}

	/**
	 * @see AbstractViewModel#onSaveInstanceState(Bundle)
	 */
	@Override
	public void onSaveInstanceState(@NonNull Bundle bundle) {
		super.onSaveInstanceState(bundle);
	}


	/**
	 * Called when view is fully created an visible to user.
	 */
	public void onViewCreated() {
		// NOTE: Can be overridden by inheritors.
	}

}

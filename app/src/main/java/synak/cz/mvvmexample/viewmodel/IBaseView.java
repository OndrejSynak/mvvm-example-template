package synak.cz.mvvmexample.viewmodel;

import android.view.View;

import eu.inloop.viewmodel.IView;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Base interface for all views.
 */

public interface IBaseView
		extends IView {

	void showKeyboard(View view);

	void hideKeyboard();

	void clearFocus();

	void finish();
}

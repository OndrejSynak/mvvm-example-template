package synak.cz.mvvmexample.viewmodel;

import android.os.Bundle;
import android.support.annotation.Nullable;

import synak.cz.mvvmexample.repository.IBaseRepository;

/**
 * Created by Ondrej Synak on 7/13/17.
 * <p>
 * Base class for view model classes which has also data layer.
 */

public abstract class RepositoryViewModel<TView extends IBaseView, TRepository extends IBaseRepository>
		extends BaseActivityViewModel<TView> {

	/* Private Attributes *************************************************************************/

	private TRepository mDataRepository;

	/* Public Methods *****************************************************************************/

	public void onCreate(@Nullable final Bundle arguments, @Nullable final Bundle savedInstanceState, TRepository repository) {
		setDataRepository(repository);

		super.onCreate(arguments, savedInstanceState);
	}

	/**
	 * Sets data repository for data layer.
	 *
	 * @param repository
	 */
	public void setDataRepository(TRepository repository) {
		this.mDataRepository = repository;
	}

	public TRepository getDataRepository() {
		return mDataRepository;
	}

}
